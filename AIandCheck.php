<?php
$your_GPTZero_API_key="";

//调试完毕后要删去以下一句
ini_set("display_errors", "On");

include "user.php";
session_start();
if(
    isset($_SESSION["user"])
    &&$_SESSION["user"] instanceof user
    &&$_SESSION["user"]->statue()
    )
{
    class docu{public $document;};
    $body=new docu();
    $post=file_get_contents('php://input');
    ///////////////////////////////////////////////////////////////
    //$post是提交的文本（就是纯文本，例如问了“你好”，那$post的值就是“你好”）
    //把AI回复的纯文本赋值给$body->document
    //请在这里补全（并把下一行代码删去）
    $body->document=$post;
    ///////////////////////////////////////////////////////////////
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://api.gptzero.me/v2/predict/text");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($body));
    
    if($your_GPTZero_API_key==""){curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));}
    else{curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-Api-Key: '.$your_GPTZero_API_key, 'Content-Type: application/json'));}

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec ($ch);

    curl_close ($ch);

    echo $server_output;
}
else{
    session_unset();
    session_destroy();
    echo '{"error":[{"msg":"not authorized"}]}';
}    
?>