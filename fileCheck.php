<?php
$your_GPTZero_API_key="";


include "user.php";
session_start();
if(
    isset($_SESSION["user"])
    &&$_SESSION["user"] instanceof user
    &&$_SESSION["user"]->statue()
    )
{
    if ($_FILES["file"]["size"] < 1048576   /* 小于 1MB*/){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://api.gptzero.me/v2/predict/files");
        curl_setopt($ch, CURLOPT_POST, 1);
        class docu{public $document;};
        $body=new docu();
        $body->document=file_get_contents('php://input');
        curl_setopt($ch, CURLOPT_POSTFIELDS,array('files' => '@' . realpath($_FILES["file"]["tmp_name"])));
        
        if($your_GPTZero_API_key==""){curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));}
        else{curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-Api-Key: '.$your_GPTZero_API_key, 'Content-Type: multipart/form-data'));}

        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        echo $server_output;

    }
    else{
        echo '{"error":[{"msg":"the file is too large."}]}';
    }
}
else{
    session_unset();
    session_destroy();
    echo '{"error":[{"msg":"not authorized"}]}';
}    
?>