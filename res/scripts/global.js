/*global.js*/

// var title=document.getElementsByTagName("title")[0];
var page = document.getElementById("mainPage");
var header = page.getElementsByTagName("header")[0];
var nav = page.getElementsByTagName("nav")[0];
var section = page.getElementsByTagName("section");

var passwordContainer=document.getElementById("passwordInput");
var passwordCancel=passwordContainer.getElementsByClassName("abandon")[0];
var password=passwordContainer.getElementsByTagName("input")[0];
var passwordCounter=document.getElementById("passwordCounter").childNodes[0];

var inputArea=document.getElementById("inputArea");
var docInput=document.getElementById("docInput");
var fileInput=document.getElementById("fileInput");
var resultArea=document.getElementById("resultArea");

var vsitorCounter=document.getElementById("vsitorCounter");

// 加载页
var loadingStatus = true;//true-项目在加载 false-加载完毕
var docuLoaded = false;//true-窗体加载完毕 false-窗体正在加载
function loading(){
    if(!loadingStatus){
        document.body.className="loading";
        loadingStatus = true;
    }
}
function loaded(){
    if(loadingStatus&&docuLoaded){
        document.body.className="loaded";
        loadingStatus = false;
    }
}
window.onload=function(){
    docuLoaded=true;
    loaded();
};

//密码框 虽然简单，考虑到可能要做动画，所以还是写两个专门函数
var passwordShown=false;
function showPasswordInput(){
    passwordShown=true;
    passwordContainer.classList.remove("hiden");
    passwordContainer.classList.add("shown");
    password.focus();
}
function hidePasswordInput(/* 此方法被Client调用，不需要直接调用*/){
    passwordShown=false;
    passwordContainer.classList.remove("shown");
    passwordContainer.classList.add("hiden");
    passwordCounter.innerHTML="0";
    password.value="";//不留值原则
}
function passKeyPress(event){
    if(event.keyCode==13){
        if(userStatus){setPassword();}
        else{verify();}
    }
    else if(event.keyCode==27){
        hidePasswordInput();
    }
}
password.addEventListener("keypress",passKeyPress);
passwordCancel.onclick=hidePasswordInput;
password.oninput=function(){
    passwordCounter.innerHTML=password.value.length+"";
};

// 滚动功能
// var baseTop;
// function rollTo(index){
//     baseTop=header.offsetHeight+112.6+15;
//     let dest=baseTop;
//     for(let i=0;i<index;i++){
//         dest+=section[i].offsetHeight+15;
//     }
//     window.scroll({top: dest, behavior: 'smooth'});
// }

//切换模块
var testType=1;//1-文本；2-文件 3-AI对话
function changeTest(){
    if(testType==1){
        inputArea.classList.remove("textTest");
        inputArea.classList.add("fileTest");
        testType=2;
    }
    else if(testType==2){
        inputArea.classList.remove("fileTest");
        inputArea.classList.add("AItalk");
        clearResults();
        testType=3;
    }
    else if(testType==3){
        inputArea.classList.remove("AItalk");
        inputArea.classList.add("textTest");
        clearResults();
        testType=1;
    }
}

//文本框自动跳高度
var docInputFocus=false,docInputHeight=50;
docInput.addEventListener('focus', (event) => {
    docInputFocus=true;
    if(docInput.scrollHeight>docInputHeight){docInputHeight=docInput.scrollHeight;}
    docInput.style.height=docInputHeight+"px";
    docInput.classList.add("focused");
});
docInput.addEventListener('blur', (event) => {
    docInputFocus=false;
    docInputHeight=50;
    docInput.style.height=docInputHeight+"px";
    docInput.classList.remove("focused");
});
docInput.onkeyup=function(){
    if(docInputFocus){
        if(docInput.scrollHeight>docInputHeight){docInputHeight=docInput.scrollHeight;}
        docInput.style.height=docInputHeight+"px";
    }
}

console.log("global.js已经成功执行到尾部");