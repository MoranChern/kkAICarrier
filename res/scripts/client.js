// client.js
var userData;
var userStatus/*true表示已登录*/=false;
var dataStatus/*true表示已经得到结果*/=false;
function correctBar(/*根据是否登录给出正确的界面*/){
    if(userStatus){
        page.classList.remove("out");
        page.classList.add("in");
    }
    else{
        page.classList.remove("in");
        page.classList.add("out");
    }
    if(dataStatus){
        page.classList.remove("notSynced");
        page.classList.add("synced");
    }
    else{
        page.classList.remove("synced");
        page.classList.add("notSynced");
        // title.innerHTML="个人简历";
        // fullName.innerHTML="请同步更改";
    }
}

function clearResults(){
    resultArea.innerHTML="";
    if(dataStatus){
        dataStatus=false;
        correctBar();
    }
}

function sayHello(/*检查当前是否已经登录*/){
    let reqest= new XMLHttpRequest();
    reqest.open("GET","./hello.php",false);
    reqest.send();
    let hello = JSON.parse(reqest.responseText);
    userStatus=hello.status;
    vsitorCounter.innerHTML=hello.count;
}

function verify(){
    loading();// 进入挂起模式
    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(result){
            userStatus=true;
            correctBar();
        }
        else{
            userStatus=false;
            correctBar();
            alert("登录失败！");
        }
        loaded();
    }
    }
    reqest.open("POST","./login.php",true);
    reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    reqest.send("password="+hex_md5(password.value));
    hidePasswordInput(); //隐藏对话框会清除值，所以放在最后
}

function logout(/*注销*/){
    loading();// 进入挂起模式
    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(!result){
            alert("非法访问！");
        }
        userStatus=false;
        // if(!dataStatus){getData();}
        correctBar();
        loaded();
    }
    }
    reqest.open("GET","./logout.php",true);
    reqest.send();
}

function setPassword(){
    if(userStatus)/*本地检查*/{
    loading();// 进入挂起模式

    let reqest= new XMLHttpRequest();
    reqest.onreadystatechange=function(){
    if (reqest.readyState==4 && reqest.status==200){
        var result=JSON.parse(reqest.responseText)
        if(result){
            alert("修改密码成功！");}
        else{
            userStatus=false;
            correctBar();
            alert("非法访问，同步失败！");
        }
        loaded();
    }
    }
    reqest.open("POST","./setPassword.php",true);
    reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    reqest.send("password="+hex_md5(password.value));

    }
    hidePasswordInput(); //隐藏对话框会清除值，所以放在最后
}

function zeroCheck(/*发起查询*/){
    if(userStatus)/*本地检查*/{

        loading();// 进入挂起模式
        let reqest= new XMLHttpRequest();
        reqest.onreadystatechange=function(){
            var allResult,status=-1/*0-成功，1-返回error，2-返回其他*/;
            if (reqest.readyState==4 && reqest.status==200){
                try{
                    allResult=JSON.parse(reqest.responseText);
                    if(Reflect.has(allResult,"error")){status=1;}
                    else{status=0;}
                }
                catch(e){
                    // console.log("发生错误！错误名称"+e.name);
                    // console.log("报错信息："+e.message);
                    status=2;            
                }        
                //////////////////////
            }
            else if(reqest.readyState==4){
                try{
                    allResult=JSON.parse(reqest.responseText);
                    if(Reflect.has(allResult,"error")){
                        status=1;
                    }
                    else{
                        allResult={error:[{msg:reqest.status+"错误"}]};
                        status=1;
                    }
                }
                catch(e){
                    status=2;
                }
            }
            ////////////////////////////////////////
            if(status==2){
                allResult={error:[{msg:"未知错误！"}]};
            }
            if(status==1||status==2){
                console.log("后端返回如下：");
                console.log(reqest.responseText);
                addError(allResult);
                loaded();
            }
            if(status==0){
                addData(allResult,testType);
                loaded();
                docInput.value="";
                fileInput.value="";
                // correctBar();
            }
        }
        //////////////////////////////////////////
        if(testType==1){
            if(docInput.value!=""){
                reqest.open("POST","./textCheck.php",true);
                reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                reqest.send(docInput.value);
            }
            else{
                addError({error:[{msg:"待检测文本为空！"}]});
                loaded();
            }
        }
        else if(testType==2){
            if(fileInput.value!=""){
                let fileData=new FormData();
                fileData.append("file",fileInput.files[0]);
                reqest.open("POST","./fileCheck.php",true);
                reqest.send(fileData);
            }
            else{
                addError({error:[{msg:"未选择待检测文件！"}]});
                loaded();
            }
        }
        else if(testType==3){
            if(docInput.value!=""){
                reqest.open("POST","./AIandCheck.php",true);
                reqest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                reqest.send(docInput.value);
                addQuestion(docInput.value);
            }
            else{
                addError({error:[{msg:"待检测文本为空！"}]});
                loaded();
            }
        }
        else{
            console.log("testType值有误，导致无法发起检测，其值为"+testType);
        }
        dataStatus=true;
        correctBar();
    }
    else if(!userStatus){
        correctBar();
        alert("非法访问！");
        loaded();
    }
}
function addQuestion(text){
    resultArea.insertBefore(document.createElement("section"),resultArea.firstChild);  
    resultArea.firstChild.className="quesBox box";
    resultArea.firstChild.innerHTML="<p></p>";
    resultArea.firstChild.lastChild.innerText=text;
}
function addError(allResult){
    resultArea.insertBefore(document.createElement("section"),resultArea.firstChild);  
    resultArea.firstChild.className="errorBox box";
    resultArea.firstChild.innerHTML="<h1>发生错误！</h1><p></p>";
    resultArea.firstChild.lastChild.innerText=allResult.error[0].msg;
}
function addData(allResult,type,i=0){
    let result=allResult.documents[i];
    //////////////////////
    resultArea.insertBefore(document.createElement("section"),resultArea.firstChild);
    if(type==3){
        resultArea.firstChild.className="replyBox resultBox box";
    }
    else if(type==1){
        resultArea.firstChild.className="resultBox box";
    }
    resultArea.firstChild.innerHTML='<div class="aveProb keyPair titleKey"><span class="keyName">平均生成概率</span><span class="keyValue">'+result.average_generated_prob+'</span></div><div class="cmpProb keyPair titleKey"><span class="keyName">全文完全生成概率</span><span class="keyValue">'+result.completely_generated_prob+'</span></div><div class="ovaBurs keyPair titleKey"><span class="keyName">全文突发性</span><span class="keyValue">'+result.overall_burstiness+'</span></div><article></article>';    
    //所有段落
    for(let i=0,iLen=result.paragraphs.length,thisPara;i<iLen;i++){
        resultArea.firstChild.children[3].appendChild(document.createElement("p"));
        thisPara=resultArea.firstChild.children[3].lastChild;
        //段生成概率
        thisPara.appendChild(document.createElement("div"));
        thisPara.lastChild.className="paraSum";
        if(result.paragraphs[i].completely_generated_prob<0.3){
            thisPara.className="probLv1";
        }
        else if(result.paragraphs[i].completely_generated_prob<0.7){
            thisPara.className="probLv2";
        }
        else{
            thisPara.className="probLv3";
        }
        thisPara.lastChild.innerHTML='<div class="keyPair"><span class="keyName">段落完全生成概率</span><span class="keyValue">'+result.paragraphs[i].completely_generated_prob+'</span></div>';
        //段落所有句子
        for(let j=result.paragraphs[i].start_sentence_index,jLim=j+result.paragraphs[i].num_sentences;j<jLim;j++){
            //单个句子
            thisPara.appendChild(document.createElement("div"));
            //绑定颜色
            // console.log("i="+i+" j="+j);
            if(result.sentences[j].generated_prob<0.3){
                thisPara.lastChild.className="ducuSent probLv1";
            }
            else if(result.sentences[j].generated_prob<0.7){
                thisPara.lastChild.className="ducuSent probLv2";
            }
            else{
                thisPara.lastChild.className="ducuSent probLv3";
            }
            ////写句子
            thisPara.lastChild.innerText=result.sentences[j].sentence;
            //写标签
            thisPara.lastChild.appendChild(document.createElement("div"));
            thisPara.lastChild.lastChild.className="sentLabel";
            thisPara.lastChild.lastChild.innerHTML='<div class="sentProb keyPair"><span class="keyName">句生成概率</span><span class="keyValue">'+result.sentences[j].generated_prob+'</span></div><div class="perplexity keyPair"><span class="keyName">句困惑度</span><span class="keyValue">'+result.sentences[j].perplexity+'</span></div>';
        }
    }
}

console.log("client.js已经成功执行到尾部");

//init
sayHello();
correctBar();
console.log("已经成功初始化");