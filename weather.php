<?php
$your_amap_API_key="";//在这里填自己的高德key
if($your_amap_API_key==""){
    echo '{"status":"0","info":"后端未设置密钥，请在weather.php的第2行设置"}';
}
else if(!isset($_POST["ip"])){die();/*非法访问*/}
else{
    $ip=$_POST["ip"];
    $ch1 = curl_init();
    curl_setopt($ch1, CURLOPT_URL,"https://restapi.amap.com/v3/ip?ip=" . $ip ."&key=" . $your_amap_API_key);
    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
    $place=json_decode(curl_exec($ch1));
    curl_close($ch1);
    if(is_string($place->adcode)){
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL,"https://restapi.amap.com/v3/weather/weatherInfo?city=" . $place->adcode . "&key=" . $your_amap_API_key);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        echo curl_exec($ch2);
        curl_close($ch2);
    }
    else{
        echo '{"status":"0","info":"定位失败"}';
    }
}
?>